
**Требования к SOFTWARE**
- MacOs
- Windows
- Linux
---
**Стек технологий**
- IntelliJ IDEA Ultimate
- Java SE Development 8
- Apache Maven version 4.0.0
- Git
- UUID
- MANIFEST.MF
- MVC(repository, service)
- MD5
- ServiceLocator
- Анотации JetBrains
- Библиотека Lombok
- Enumeration
- Сериализация\Десериализация
- JAX-B, JSON, XML, FASTERXML
- PostgreSQL
- DBeaver
- JDBC
- MyBatis (Annotations)
- JPA + Hibernate
- Weld CDI
- Spring Core
- Spring Data JPA
- Spring MVC
- Spring Boot
- Spring Security
---

**Разработчик**

Тагиров Ренат Айратович <br>
[rena.tagirov@gmail.com](mailto:rena.tagirov@gmail.com)
---

**Для сборки приложения выполнить команду:**
```
mvn install
```
---
**Для запуска программы в консоли выполнить команду:**
```
java -jar task-manager-19.0.0.jar
```
