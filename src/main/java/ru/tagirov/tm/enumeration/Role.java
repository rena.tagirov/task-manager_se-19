package ru.tagirov.tm.enumeration;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import java.util.Set;
import java.util.stream.Collectors;


public enum Role {

    ADMINISTRATOR("administrator", Set.of("user:read", "user:write")),
    USER("user", Set.of("user:read"));


    @NotNull
    @Getter
    private final String name;

    @NotNull
    @Getter
    private final Set<String> permissions;

    Role(@NotNull String name, @NotNull Set<String> permission) {
        this.name = name;
        this.permissions = permission;
    }

    public Set<SimpleGrantedAuthority> getAuthorities() {
        return getPermissions().stream().map(SimpleGrantedAuthority::new)
                .collect(Collectors.toSet());
    }
}
