package ru.tagirov.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.tagirov.tm.util.EntityCreateUtil;

import java.sql.SQLException;
import java.text.ParseException;

@RestController
public class CreateDataBaseController {

    @Autowired
    private EntityCreateUtil entityCreateUtil;

    @GetMapping
    public String createUser() throws SQLException, ParseException {
        entityCreateUtil.create();
        return "create - ok!";
    }
}
