package ru.tagirov.tm.controller;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.tagirov.tm.api.service.ProjectService;
import ru.tagirov.tm.dto.ProjectDto;
import ru.tagirov.tm.entity.Project;
import ru.tagirov.tm.enumeration.PurposeComparator;

import java.sql.SQLException;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/project",produces =  MediaType.APPLICATION_JSON_VALUE)
public class ProjectController {

    @Autowired
    private ProjectService projectService;

    @Autowired
    private ModelMapper mapper;

    @GetMapping
    public ResponseEntity<List<ProjectDto>> findAll() throws SQLException {
        List<Project> projects = projectService.findAll();
        List<ProjectDto> projectDtos = projects.stream().map(e -> mapper.map(e, ProjectDto.class)).collect(Collectors.toList());
        return new ResponseEntity<>(projectDtos, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<ProjectDto> save(@RequestBody ProjectDto projectDto) throws SQLException {
        Project project = mapper.map(projectDto, Project.class);
        Project project1 = projectService.save(project);
        ProjectDto projectDto1 = mapper.map(project1, ProjectDto.class);
        return new ResponseEntity<>(projectDto1, HttpStatus.OK);
    }

    @PutMapping
    public ResponseEntity<ProjectDto> update(@RequestBody ProjectDto projectDto) throws SQLException {
        Project project = mapper.map(projectDto, Project.class);
        Project project1 = projectService.save(project);
        ProjectDto projectDto1 = mapper.map(project1, ProjectDto.class);
        return new ResponseEntity<>(projectDto1, HttpStatus.OK);
    }

    @DeleteMapping
    public ResponseEntity<ProjectDto> deleteAll() throws SQLException {
        projectService.removeAll();
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<ProjectDto> deleteById(@PathVariable("id") String id) throws SQLException {
        Project project = projectService.getOne(id);
        projectService.remove(project);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<ProjectDto> getOne(@PathVariable("id") String id) throws SQLException {
        Project project = projectService.getOne(id);
        ProjectDto projectDto = mapper.map(project, ProjectDto.class);
        return new ResponseEntity<>(projectDto, HttpStatus.OK);
    }

    @GetMapping("/findAllByUserId/{id}")
    public ResponseEntity<List<ProjectDto>> findAllByUserId(@PathVariable("id") String id) throws SQLException {
        List<Project> list = projectService.findAllByUserId(id);
        List<ProjectDto> projectDtos = list.stream().map(e -> mapper.map(e, ProjectDto.class)).collect(Collectors.toList());
        return new ResponseEntity<>(projectDtos, HttpStatus.OK);
    }

    @DeleteMapping("/deleteAllByUserId/{id}")
    public ResponseEntity<ProjectDto> deleteAllByUserId(@PathVariable("id") String id) throws SQLException {
        projectService.deleteAllByUser_Id(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping("/setProjectList")
    public ResponseEntity<List<ProjectDto>> setProjectList(@RequestBody List<ProjectDto> listDto) throws SQLException {
        List<Project> list = listDto.stream().map(e -> mapper.map(e, Project.class)).collect(Collectors.toList());
        List<ProjectDto> listDto1 = list.stream().map(e -> mapper.map(e, ProjectDto.class)).collect(Collectors.toList());
        return new ResponseEntity<>(listDto1, HttpStatus.OK);
    }

    @GetMapping("/findAllBySort/{sort}")
    public ResponseEntity<List<ProjectDto>> findAllBySort(@PathVariable("sort") String sort) throws SQLException {
        List<Project> list = projectService.findAll();
        switch (sort) {
            case "1":
                list.sort(PurposeComparator.DATE_BEGIN_COMPARATOR.getComparator());
                break;
            case "2":
                list.sort(PurposeComparator.DATE_END_COMPARATOR.getComparator());
                break;
            case "3":
                list.sort(PurposeComparator.STATUS_COMPARATOR.getComparator());
                break;
            default:
                System.out.println("Insertion order!");
        }
        List<ProjectDto> listDto1 = list.stream().map(e -> mapper.map(e, ProjectDto.class)).collect(Collectors.toList());
        return new ResponseEntity<>(listDto1, HttpStatus.OK);
    }
}
