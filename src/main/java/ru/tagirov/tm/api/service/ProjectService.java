package ru.tagirov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tagirov.tm.entity.Project;

import java.sql.SQLException;
import java.util.List;


public interface ProjectService {

    //    CRUD ----------------------------------------------------------------------

    Project save(@NotNull final Project project) throws SQLException;

    @Nullable
    Project getOne(@NotNull final String projectId) throws SQLException;

    @NotNull
    List<Project> findAll() throws SQLException;

    void remove(@NotNull final Project project) throws SQLException;

    void removeAll() throws SQLException;

    //    ALL ------------------------------------------------------------------------

    @Nullable
    List<Project> findAllByUserId(@NotNull final String userId) throws SQLException;

    void deleteAllByUser_Id(@NotNull String userId);

}
