package ru.tagirov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.tagirov.tm.entity.User;

import java.sql.SQLException;
import java.util.List;


public interface UserService {

    User save(@NotNull final User user) throws SQLException;

    @NotNull
    List<User> findAll() throws SQLException;

    void delete(@NotNull final User user) throws SQLException;

    void deleteAll() throws SQLException;

    User findByLogin(@NotNull String login);

    User getOne(@NotNull String id);

}
