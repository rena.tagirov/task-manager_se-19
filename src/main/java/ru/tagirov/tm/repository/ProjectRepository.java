package ru.tagirov.tm.repository;

//public class ProjectRepository implements IProjectRepository {
//
//    private EntityManager entityManager;
//
//    public void setEntityManager(@NotNull EntityManager entityManager) {
//        this.entityManager = entityManager;
//    }
//
//    //    CRUD ----------------------------------------------------------------------
//
//    public void getPrint(){}
//    @Override
//    public void persist(@NotNull Project project) throws SQLException {
//        entityManager.persist(project);
//    }
//
//    @Override
//    public void merge(@NotNull Project project) throws SQLException {
//        entityManager.merge(project);
//    }
//
//    @Override
//    public @Nullable Project findOne(@NotNull String uuid) throws SQLException {
//        return entityManager.find(Project.class, uuid);
//    }
//
//    @Override
//    public @NotNull Collection<Project> findAll() throws SQLException {
//        return entityManager.createQuery("SELECT u FROM Project as u", Project.class).getResultList();
//    }
//
//    @Override
//    public void remove(@NotNull final Project project) throws SQLException {
//        @NotNull final Project newProject = entityManager.find(Project.class, project.getId());
//        entityManager.remove(newProject);
//    }
//
//    @Override
//    public void removeAll() throws SQLException {
//        entityManager.createQuery("DELETE FROM Project p", Project.class).executeUpdate();
//    }
//
//    //    ALL ------------------------------------------------------------------------
//
//    @Override
//    public @Nullable Project findOneByUserId(@NotNull String userId, @NotNull String uuid) throws SQLException {
//        return entityManager.createQuery("SELECT p FROM Project p WHERE p.user_id =: user_id AND p.id = : id", Project.class).
//                setParameter("user_id", userId)
//                .setParameter("id", uuid)
//                .getSingleResult();
//    }
//
//    @Override
//    public @Nullable Collection<Project> findAllByUserId(@NotNull String userId) throws SQLException {
//        return entityManager.createQuery(
//                "SELECT p FROM Project p WHERE p.userId = :user_id", Project.class)
//                .setParameter("user_id",userId)
//                .getResultList();
//    }
//
//    @Override
//    public void removeByUserId(@NotNull String userId, @NotNull String uuid) throws SQLException {
//        @NotNull final User user = entityManager.find(User.class,userId);
//        entityManager.createQuery("Delete from Project p where p.user = :userId and id = :id")
//                .setParameter("user", user)
//                .setParameter("id", uuid)
//                .executeUpdate();
//    }
//
//    @Override
//    public void removeAllByUserId(@NotNull String userId) throws SQLException {
//        @NotNull final User user = entityManager.find(User.class,userId);
//        @NotNull final List<Project> projectList = entityManager.createQuery("Select p from Project p where user = :user", Project.class)
//                .setParameter("user", user).getResultList();
//        for (@NotNull final Project project : projectList) {
//            @NotNull final Project newProject = entityManager.find(Project.class, project.getId());
//            entityManager.remove(newProject);
//        }
//    }
//
//}
