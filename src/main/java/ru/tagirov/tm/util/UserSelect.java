package ru.tagirov.tm.util;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tagirov.tm.api.service.UserService;
import ru.tagirov.tm.entity.User;
import ru.tagirov.tm.terminal.TerminalService;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Component
public class UserSelect {

    @Autowired
    private UserService userService;

    @Autowired
    private TerminalService terminalService;

    public User getUser(){
        @NotNull List<User> users = null;
        try {
            users = new ArrayList<>(userService.findAll());
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        System.out.println("CHOOSE THE RIGHT USER: ");
        int count = 1;
        for (User user : users) {
            System.out.print(count + ": " );
            System.out.println(user.toString());
            count++;
        }
        final int numberUser = terminalService.parseInt();
        User user = users.get(numberUser - 1);
        return user;
    }
}
