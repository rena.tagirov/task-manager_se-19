package ru.tagirov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tagirov.tm.api.repository.UserRepository;
import ru.tagirov.tm.api.service.UserService;
import ru.tagirov.tm.entity.User;

import java.util.List;

@Service
@Transactional
public class UserServiceImpl implements UserService {

    @Autowired
    UserRepository userRepository;

    @Override
    public User save(@NotNull User user) {
        return userRepository.save(user);
    }

    @Override
    public @NotNull List<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public void delete(@NotNull final User user) {
        userRepository.delete(user);
    }

    @Override
    public void deleteAll() {
        userRepository.deleteAll();
    }

    @Override
    public User findByLogin(@NotNull String login) {
        return userRepository.findByLogin(login);
    }

    public User getOne (@NotNull String id){
        return userRepository.getOne(id);
    }

}
